from flask import Flask,render_template,url_for,request
import hashlib as h
app = Flask(__name__)
@app.route('/')
def hello_world():
    a=h.algorithms_guaranteed
    b=list(a)
    c={b[i]:i for i in range(0,len(b))}
    return render_template('home.html', a1=c)
@app.route('/hashing',methods=['POST'])
def ash1():
    a=h.algorithms_guaranteed
    b=list(a)
    c={b[i]:i for i in range(0,len(b))}
    u = request.form['hashalg']
    data1=request.form['hash2']
    if(u=="md5"):hashgen=h.md5(data1.encode()).hexdigest()
    elif(u=="sha224"):hashgen=h.sha224(data1.encode()).hexdigest()
    elif(u=="sha1"):hashgen=h.sha1(data1.encode()).hexdigest()
    elif(u=="sha512"):hashgen=h.sha512(data1.encode()).hexdigest()
    elif(u=="sha256"):hashgen=h.sha256(data1.encode()).hexdigest()
    elif(u=="sha384"):hashgen=h.sha384(data1.encode()).hexdigest()
    elif(u=="sha3_384"):hashgen=h.sha3_384(data1.encode()).hexdigest()
    elif(u=="sha3_224"):hashgen=h.sha3_224(data1.encode()).hexdigest()
    elif(u=="sha3_256"):hashgen=h.sha3_256(data1.encode()).hexdigest()
    elif(u=="sha3_512"):hashgen=h.sha3_512(data1.encode()).hexdigest()
    elif(u=="blake2b"):hashgen=h.blake2b(data1.encode()).hexdigest()
    elif(u=="blake2s"):hashgen=h.blake2s(data1.encode()).hexdigest()
    elif(u=="shake_256"):hashgen=h.shake_256(data1.encode()).hexdigest()
    elif(u=="shake_128"):hashgen=h.shake_128(data1.encode()).hexdigest()
    else:hashgen="Hash Generation Failed"
    return render_template('hash2.html', n=u,n1=data1,hashg1=hashgen,a1=c)
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'),404
if __name__ == '__main__':
    app.run(debug=True)
